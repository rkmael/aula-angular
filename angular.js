var app = angular.module("app", ['ui.bootstrap']);

app.controller("Ctrl1", function($scope, Article, $interval){

	$scope.title = "Desafio";
	$scope.limit = 5;
	$scope.postsFavorites = [];

	Article.get(function(posts){
   		$scope.posts = posts.data.children;
	});

	$scope.define_limit = function(limit){
		$scope.limit = limit;
	}

});


app.factory("Article", function($http){
     return {
         get: function(callback){
             return $http.get("http://www.reddit.com/.json").success(callback);
		}
	}
});

app.directive("favoritar", function(){
	return {
		restrict: "E",	
		scope:{			
			post: "="
		},
		templateUrl: 'button.html',
		link: function(scope, element, attr){
	            scope.moverFavorito = function () {
	            	scope.$parent.$parent.postsFavorites.push(scope.post)
	            	scope.$parent.$parent.posts.splice( scope.$parent.$parent.posts.indexOf(scope.post), 1 );
	            };

		}

	}
});


app.directive("showPost", function(){
	return {
		restrict: "E",	
		scope:{			
			post: "="
		},
		templateUrl: 'template.html',
		replace: true

	}
});









